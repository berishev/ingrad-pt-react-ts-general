import * as React from 'react';
import * as Parse from 'parse';
import * as Model from '../../../model';
import { IPageControllerProps } from '../../../abstract/global/pageController';

import View from './view';
import executeController from '../../../abstract/global/executeController';

export interface IControllerParams {
  id?: string;
}

export interface IControllerProps
  extends IPageControllerProps<IControllerParams, Model.Team> {}

interface IControllerState {
  item: Model.Team;
}

export default class Controller extends React.Component<
  IControllerProps,
  IControllerState
> {
  constructor(props: IControllerProps) {
    super(props);
    this.state = {
      item: new Model.Team()
    };
  }

  componentDidMount() {
    executeController.tryLoad(this.load);
  }

  load = async () => {
    let { id } = this.props.controller.navigator.params;
    let { item } = this.state;
    if (id) item = await new Parse.Query(Model.Team).get(id);
    this.setState({ item });
  };

  save = async () => {
    let { item } = this.state;
    item = await item.save();
    this.setState({ item });
    return this.props.modal.resolve(item);
  };

  cancel = () => {
    return this.props.modal.reject();
  };

  // VIEW

  onChange = (item: Model.Team) => {
    if (!item.fullname && item.spOrg) item.fullname = item.spOrg.LinkTitle;
    this.setState({ item });
  };
  onSave = () => executeController.tryLoadNotification(this.save);
  onCancel = () => executeController.tryLoad(this.cancel);

  render() {
    return <View controller={this} />;
  }
}
