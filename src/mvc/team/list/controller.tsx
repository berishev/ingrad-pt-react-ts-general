import * as React from 'react';
import Decorators from 'berish-decorate';
import * as Parse from 'parse';
import * as Model from '../../../model';
import Form from '../form/controller';
import {
  DRouteQuery,
  DPublicRoute
} from '../../../abstract/util/decorators/route';
import { IPageControllerProps } from '../../../abstract/global/pageController';

import View from './view';
import storageController from '../../../abstract/global/storageController';
import executeController from '../../../abstract/global/executeController';

export interface IControllerProps extends IPageControllerProps {}

interface IControllerState {
  query: Parse.Query<Model.Team>;
  selected: Model.Team[];
}

export default class Controller extends React.Component<
  IControllerProps,
  IControllerState
> {
  constructor(props: IControllerProps) {
    super(props);
    this.state = {
      query: Model.Team.getQuery(),
      selected: []
    };
  }

  componentDidMount() {
    executeController.tryLoad(this.load);
  }

  load = async () => {
    const systemStore = storageController.systemStore;
    await systemStore.dispatch(
      systemStore.createMethod(m => {
        m.title = 'Отделы';
      })
    );
    let { query } = this.state;
    query = Model.Team.getQuery();
    this.setState({ query });
  };

  remove = async () => {
    let { selected } = this.state;
    await Promise.all(selected.map(m => m.destroy()));
  };

  // VIEW

  onSelect = (selected: Model.Team[]) => this.setState({ selected });

  onAdd = async () => {
    let item = await this.props.controller.navigator.pushModal(Form)();
    if (item) this.setState({ query: Model.Team.getQuery() });
  };
  onEdit = async () => {
    let item = await this.props.controller.navigator.pushModal(Form)({
      id: this.state.selected[0].id
    });
    console.log(item);
    if (item) this.setState({ query: Model.Team.getQuery() });
  };
  onRemove = async () => {
    await executeController.tryLoad(this.remove);
    this.setState({ query: Model.Team.getQuery(), selected: [] });
  };

  render() {
    return <View controller={this} />;
  }
}
