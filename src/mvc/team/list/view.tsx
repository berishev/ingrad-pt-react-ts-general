import * as React from 'react';
import * as Model from '../../../model';
import * as Components from '../../../abstract/components';
import { IColumn } from '../../../abstract/components/table/view';
import Controller from './controller';
import storageController from '../../../abstract/global/storageController';

export interface IViewProps {
  controller: Controller;
}

export default class extends React.Component<IViewProps> {
  constructor(props: IViewProps) {
    super(props);
  }

  renderColumns = () => {
    let columns: IColumn<Model.Team>[] = [
      {
        title: 'Название отдела',
        render: item => item.name
      },
      {
        title: 'Полное наименование отдела',
        render: item => item.fullname
      },
      {
        title: 'ИД',
        render: item => item.spOrg && item.spOrg.spID,
        width: 30
      }
    ];
    return columns;
  };

  render() {
    let { state, onSelect } = this.props.controller;
    let { query, selected } = state;
    return (
      <Components.ContentForm>
        <Components.Table.QueryTable
          data={query}
          columns={this.renderColumns()}
          selection={{ items: selected, on: onSelect, disabled: true }}
          columnStyle={{ fontSize: 19 }}
          rowStyle={{ fontSize: 17 }}
        />
      </Components.ContentForm>
    );
  }
}
