import * as React from 'react';
import Enum from 'berish-enum';
import * as collection from 'berish-collection';
import { Root } from 'berish-react-portals/dist';
import { EnumValueOf } from 'berish-enum/dist/lib';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';

import {
  apiController,
  messagesController,
  storageController
} from '../abstract/global';
import * as Components from '../abstract/components';
import controllers from './modules';
import { PageController } from '../abstract/global/pageController';

const AppTypeEnum = Enum.createFromPrimitive('teamUser', 'project', 'team');

const AppTypeLabels = new collection.Dictionary<
  EnumValueOf<typeof AppTypeEnum>,
  string
>(
  new collection.KeyValuePair(AppTypeEnum.teamUser, 'Команды проектов'),
  new collection.KeyValuePair(AppTypeEnum.project, 'Проекты'),
  new collection.KeyValuePair(AppTypeEnum.team, 'Отделы')
);

interface IAppState {
  loaded: boolean;
  type: EnumValueOf<typeof AppTypeEnum>;
}

export default class App extends React.Component<any, IAppState> {
  constructor(props) {
    super(props);
    this.state = { loaded: false, type: AppTypeEnum.teamUser };
  }
  async componentWillMount() {
    await apiController.init();
    await storageController.localStore.load();
    this.setState({ loaded: true });
  }

  renderTab = (
    type: EnumValueOf<typeof AppTypeEnum>,
    Controller: React.ComponentClass<{ controller: PageController }>
  ) => {
    return (
      <Components.Tabs.Child
        key={type}
        label={AppTypeLabels.get(type)}
        value={type}
      >
        <Controller controller={null} />
      </Components.Tabs.Child>
    );
  };

  renderRoutes = () => {
    return (
      <Components.Tabs.Root
        value={this.state.type}
        onChange={type => this.setState({ type })}
      >
        {this.renderTab(AppTypeEnum.teamUser, controllers.teamUser.list)}
        {this.renderTab(AppTypeEnum.project, controllers.project.list)}
        {this.renderTab(AppTypeEnum.team, controllers.team.list)}
      </Components.Tabs.Root>
    );
  };

  render() {
    if (!this.state.loaded) return messagesController.loadingGlobal(true);
    return (
      <>
        {this.renderRoutes()}
        <Root />
        <ToastContainer
          position={toast.POSITION.BOTTOM_RIGHT}
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick={true}
          rtl={false}
          draggable={true}
          pauseOnHover={true}
        />
      </>
    );
  }
}
