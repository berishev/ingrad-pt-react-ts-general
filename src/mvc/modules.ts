import teamUser from './teamUser';
import project from './project';
import team from './team';

export default {
  teamUser,
  project,
  team
};
