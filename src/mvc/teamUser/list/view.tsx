import * as React from 'react';
import * as Model from '../../../model';
import { Clear } from '@material-ui/icons';
import * as Components from '../../../abstract/components';
import { IColumn } from '../../../abstract/components/table/view';
import Controller from './controller';
import { LINQ } from 'berish-linq';
import storageController from '../../../abstract/global/storageController';

export interface IViewProps {
  controller: Controller;
}

export default class extends React.Component<IViewProps> {
  constructor(props: IViewProps) {
    super(props);
  }

  renderCustomColumns = (linq: LINQ<Model.Project>) => {
    const { onFilterConfig, state } = this.props.controller;
    const { filter } = state;
    return linq
      .select(m => {
        const indexInFilter = filter.projects.indexOf(m);
        let column: IColumn<Model.TeamUser> = {
          title:
            (m.iconUrl && (
              <Components.Image
                selected={indexInFilter != -1}
                onClick={() => {
                  if (indexInFilter == -1) filter.projects.push(m);
                  else filter.projects.splice(indexInFilter, 1);
                  onFilterConfig(filter);
                }}
                style={{
                  width: 30,
                  height: 30
                }}
                placeholder={m.name || m.code}
                value={m.iconUrl}
              />
            )) ||
            m.code,
          render: item => {
            let project = LINQ.fromArray(item.projects).firstOrNull(
              k => k.id == m.id
            );
            if (project)
              return (
                (m.iconUrl && (
                  <Components.Image
                    style={{
                      width: 30,
                      height: 30
                    }}
                    placeholder={m.name || m.code}
                    value={m.iconUrl}
                  />
                )) ||
                m.code
              );
            return '';
          },
          width: 50
        };
        return column;
      })
      .toArray();
  };

  renderColumns = () => {
    const { onFilterConfig, state } = this.props.controller;
    const { filter, projects } = state;
    let mProjects = LINQ.fromArray(projects).where(
      m => m.relType == Model.ProjectRelTypeEnum.M
    );
    let moProjects = LINQ.fromArray(projects).where(
      m => m.relType == Model.ProjectRelTypeEnum.MO
    );
    let mColumns = this.renderCustomColumns(mProjects);
    let moColumns = this.renderCustomColumns(moProjects);
    let mainColumn: IColumn<Model.TeamUser>[] = [
      {
        title: 'Отдел',
        render: item =>
          item.team &&
          Components.Abstract.Tooltip(<div>{item.team.name}</div>, {
            trigger: 'hover',
            placeholder: item.team.fullname || item.team.name
          }),
        fixed: true,
        width: 100
      },
      {
        title: 'Проектная роль',
        render: item => item.position,
        fixed: true,
        width: 200
      },
      {
        title: items => `ФИО (${items.length})`,
        render: item =>
          `${item.lastname || ''} ${item.name || ''} ${item.patronymic ||
            ''}`.trim() + ` (${item.projects.length})`,
        fixed: true,
        width: 200
      },
      {
        title: 'Блок задач',
        render: item => item.task,
        width: 150
      },
      {
        title: 'Доб.',
        render: item => item.phoneCode,
        width: 100
      },
      {
        title: 'Мобильный',
        render: item => item.phoneMobile,
        width: 200
      },
      {
        title: 'Оф.',
        render: item => item.officeNumber,
        width: 70
      },
      {
        title: 'Эт/Каб.',
        render: item => item.floorOrRoom,
        width: 90
      }
    ];
    return mainColumn
      .concat(
        {
          title: (
            <Components.Image
              selected={
                filter.relType.indexOf(Model.ProjectRelTypeEnum.M) != -1
              }
              onClick={() => {
                const indexInFilter = filter.relType.indexOf(
                  Model.ProjectRelTypeEnum.M
                );
                if (indexInFilter == -1)
                  filter.relType.push(Model.ProjectRelTypeEnum.M);
                else filter.relType.splice(indexInFilter, 1);
                onFilterConfig(filter);
              }}
              style={{
                width: 30,
                height: 30
              }}
              placeholder="Москва"
              value="/images/m.png"
            />
          ),
          render: item => '',
          width: 50
        },
        ...mColumns
      )
      .concat(
        {
          title: (
            <Components.Image
              selected={
                filter.relType.indexOf(Model.ProjectRelTypeEnum.MO) != -1
              }
              onClick={() => {
                const indexInFilter = filter.relType.indexOf(
                  Model.ProjectRelTypeEnum.MO
                );
                if (indexInFilter == -1)
                  filter.relType.push(Model.ProjectRelTypeEnum.MO);
                else filter.relType.splice(indexInFilter, 1);
                onFilterConfig(filter);
              }}
              style={{
                width: 30,
                height: 30
              }}
              placeholder="Московская область"
              value="/images/mo.png"
            />
          ),
          render: item => '',
          width: 50
        },
        ...moColumns
      );
  };

  renderFilter = (index: number, item: Model.Team) => {
    const { onFilterConfig, state } = this.props.controller;
    const { filter } = state;
    const indexInFilter = filter.teams.indexOf(item);
    return (
      <div style={{ width: '100px', margin: '10px 15px' }}>
        <Components.Button
          buttonTitle={item.name}
          placeholder={item.fullname || item.name}
          variant={indexInFilter == -1 ? 'flat' : 'contained'}
          onClick={() => {
            if (indexInFilter == -1) filter.teams.push(item);
            else filter.teams.splice(indexInFilter, 1);
            onFilterConfig(filter);
          }}
        />
      </div>
    );
  };

  render() {
    const { state, onSelect, onFilterConfig, onExport } = this.props.controller;
    const { selected, teams, filter } = state;
    return (
      <Components.ContentForm>
        <Components.Row
          wrap="wrap"
          style={{ alignItems: 'center', justifyContent: 'space-between' }}
        >
          <div style={{ flex: 1 }}>
            <Components.Textfield
              config={{
                object: filter,
                path: 'search',
                afterSet: m => onFilterConfig(m)
              }}
              fiTitle="Поиск"
              placeholder="Поиск по фамилии/имени/отчеству"
            />
          </div>
          <div style={{ flex: 1 }} />
          <div style={{ flex: 1 }}>
            {filter.filters.length > 0 && (
              <Components.Button
                buttonTitle="Сбросить фильтр"
                style={{ color: 'red', fontWeight: 'bold' }}
                icon={<Clear />}
                onClick={() => {
                  onFilterConfig(filter.cleanAttributes());
                }}
              />
            )}
          </div>
          <div style={{ flex: 1 }}>
            <Components.Button
              buttonTitle="Экспорт"
              style={{ fontWeight: 'bold' }}
              onClick={() => {
                onExport();
              }}
            />
          </div>
        </Components.Row>
        <Components.Row wrap="wrap" style={{ margin: '10px 0' }}>
          {Components.ArrayComponents.render({
            template: this.renderFilter,
            elements: teams
          })}
        </Components.Row>
        <Components.Table.QueryTable
          data={filter.response}
          columns={this.renderColumns()}
          selection={{ items: selected, on: onSelect, disabled: true }}
          containerMinusHeight={450}
          columnStyle={{ fontSize: 19 }}
          rowStyle={{ fontSize: 17 }}
        />
      </Components.ContentForm>
    );
  }
}
