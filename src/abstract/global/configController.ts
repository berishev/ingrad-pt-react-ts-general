import * as ringle from 'berish-ringle';
import config from '../../config';

class ConfigController {
  get api() {
    return config.api;
  }

  get color() {
    return config.color;
  }

  get meta() {
    return config.meta;
  }

  get dimensions() {
    let body = document.getElementsByTagName('body')[0];
    let x = window.innerWidth || document.documentElement.clientWidth || body.clientWidth;
    let y = window.innerHeight || document.documentElement.clientHeight || body.clientHeight;
    return { x, y };
  }
}

export default ringle.getSingleton(ConfigController);
