import { NavigatorController } from './navigatorController';
import { IStaticComponentProps } from 'berish-react-portals/dist/lib/portal';
import { IModalRouteAdditionalProps } from '../router/modalRoute';

export interface IPageControllerProps<MatchGeneric = {}, ResolveGeneric = {}> {
  controller: PageController<MatchGeneric>;
  modal?: IStaticComponentProps<ResolveGeneric> &
    IModalRouteAdditionalProps<MatchGeneric>;
}

export class PageController<MatchGeneric = {}> {
  static init<MatchGeneric>(
    params: MatchGeneric,
    controller?: PageController<MatchGeneric>
  ) {
    if (!controller) controller = new PageController();
    return controller.receive(params);
  }

  private _navigatorController: NavigatorController<MatchGeneric> = null;

  receive(params: MatchGeneric) {
    this._navigatorController = NavigatorController.init(
      params,
      this._navigatorController
    );
    return this;
  }

  get navigator() {
    return this._navigatorController;
  }
}
