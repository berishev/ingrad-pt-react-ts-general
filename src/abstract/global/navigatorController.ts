import { IPageControllerProps } from './pageController';
import Portal from 'berish-react-portals';
import ModalRoute, { IModalRouteProps } from '../router/modalRoute';

export class NavigatorController<MatchGeneric> {
  static init<MatchGeneric>(
    params: MatchGeneric,
    controller?: NavigatorController<MatchGeneric>
  ) {
    if (!controller) controller = new NavigatorController<MatchGeneric>();
    return controller.receive(params);
  }

  private _params: MatchGeneric = null;

  receive(params: MatchGeneric) {
    this._params = params;
    return this;
  }

  get params() {
    return this._params || ({} as MatchGeneric);
  }

  pushModal<MatchGeneric, ResolveGeneric>(
    component: React.ComponentClass<
      IPageControllerProps<MatchGeneric, ResolveGeneric>
    >
  ) {
    let decorate = Portal.create(ModalRoute);
    return (params?: MatchGeneric) => {
      let props: IModalRouteProps<MatchGeneric> = {
        component,
        params
      };
      return decorate(props) as Promise<ResolveGeneric>;
    };
  }
}
