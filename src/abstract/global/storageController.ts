import { createStore } from 'redux';
import { Storage, StorageAdapters } from './redux-helper';
import * as stores from './stores';

const globalStore = new stores.GlobalStore(createStore, []);
const systemStore = new stores.SystemStore(createStore, []);

const localStorage = new Storage('localStore', StorageAdapters.LocalStorage);

const localStore = new stores.LocalStore(createStore, []).setStorage(
  localStorage
);

export default { globalStore, systemStore, localStore };
