import * as React from 'react';
import { PageController, IPageControllerProps } from '../global/pageController';
import { Modal } from '../components';
import { IStaticComponentProps } from 'berish-react-portals/dist/lib/portal';
import { storageController } from '../global';

export interface IModalRouteAdditionalProps<MatchGeneric = {}> {
  updateParams: (params?: MatchGeneric) => void;
}

export interface IModalRouteProps<MatchGeneric> {
  component: React.ComponentClass<IPageControllerProps<MatchGeneric>>;
  params: MatchGeneric;
}

interface IModalRouteState<MatchGeneric = {}> {
  controller: PageController<MatchGeneric>;
}

export default class ModalRoute<
  MatchGeneric = {},
  ResolveGeneric = {}
> extends React.PureComponent<
  IModalRouteProps<MatchGeneric> & IStaticComponentProps<ResolveGeneric>,
  IModalRouteState<MatchGeneric>
> {
  systemUnlistener = storageController.systemStore.subscribe(m =>
    this.forceUpdate()
  );
  localStoreUnlistener = storageController.localStore.subscribe(m =>
    this.forceUpdate()
  );
  globalStoreUnlistener = storageController.globalStore.subscribe(m =>
    this.forceUpdate()
  );

  constructor(props) {
    super(props);
    this.state = {
      controller: PageController.init(this.props.params)
    };
  }
  componentWillReceiveProps(nextProps: IModalRouteProps<MatchGeneric>) {
    this.setState({
      controller: PageController.init(nextProps.params, this.state.controller)
    });
  }

  componentWillUnmount() {
    if (this.systemUnlistener) this.systemUnlistener();
    if (this.localStoreUnlistener) this.localStoreUnlistener();
    if (this.globalStoreUnlistener) this.globalStoreUnlistener();
  }

  resolve = (resolveGeneric: ResolveGeneric) => {
    this.props.resolve(resolveGeneric);
  };

  reject = () => {
    this.props.resolve();
  };

  updateParams = (params?: MatchGeneric) => {
    let { controller } = this.state;
    controller = controller.receive(params);
    this.forceUpdate();
  };

  render() {
    const controller = this.state.controller;
    const ComponentClass = this.props.component;
    return (
      <Modal.Form resolve={this.resolve} reject={this.reject} width="80vw">
        <ComponentClass
          controller={controller}
          modal={{
            resolve: this.resolve,
            reject: this.reject,
            getContainer: this.props.getContainer,
            updateParams: this.updateParams
          }}
        />
      </Modal.Form>
    );
  }
}
