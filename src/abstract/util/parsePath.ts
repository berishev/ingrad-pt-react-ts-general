export function parseQueryString(query: string) {
  if (!query) return {};
  let pairs = query.split('&');
  let queryString: { [key: string]: any } = {};
  for (let i = 0; i < pairs.length; i++) {
    let pair = pairs[i].split('=');
    if (typeof queryString[pair[0]] === 'undefined') {
      queryString[pair[0]] = decodeURIComponent(pair[1]);
    } else if (typeof queryString[pair[0]] === 'string') {
      let arr = [queryString[pair[0]], decodeURIComponent(pair[1])];
      queryString[pair[0]] = arr;
    } else {
      queryString[pair[0]].push(decodeURIComponent(pair[1]));
    }
  }
  return queryString;
}
