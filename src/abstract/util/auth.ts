import * as Parse from 'parse';
import { storageController } from '../global';

export type userType = Parse.User;
export type roleType = Parse.Role;

export async function getCurrentUser() {
  let storeUser = storageController.globalStore.user;
  let currentUser = Parse.User.current() as Parse.User;
  if (
    storeUser == null ||
    currentUser == null ||
    storeUser.id != currentUser.id
  ) {
    if (currentUser == null)
      await storageController.globalStore.dispatch(
        storageController.globalStore.createMethod(m => {
          m.user = null;
        })
      );
    else {
      try {
        let serverUser = await new Parse.Query(Parse.User)
          .include('permissionRole')
          .get(currentUser.id);
        await storageController.globalStore.dispatch(
          storageController.globalStore.createMethod(m => {
            m.user = serverUser;
          })
        );
      } catch (err) {
        console.log(err);
        await logOut();
      }
    }
  }
  return storageController.globalStore.user;
}

export async function getSessionToken() {
  let user = Parse.User.current();
  return user && user.getSessionToken();
}

export async function getCurrentRole() {
  let user = await getCurrentUser();
  let storeRole = storageController.globalStore.role;
  let currentRole = user && user['role'];
  if (
    storeRole == null ||
    currentRole == null ||
    storeRole.id != currentRole.id
  ) {
    if (currentRole == null)
      await storageController.globalStore.dispatch(
        storageController.globalStore.createMethod(m => {
          m.role = null;
        })
      );
    else {
      let serverRole = await new Parse.Query(Parse.Role).get(currentRole.id);
      await storageController.globalStore.dispatch(
        storageController.globalStore.createMethod(m => {
          m.role = serverRole;
        })
      );
    }
  }
  return storageController.globalStore.role;
}

export async function isLogged() {
  let current = await getCurrentUser();
  let session = await getSessionToken();
  let isAuthenticated = !!current;
  let isValidSession = isAuthenticated && !!session;
  return isAuthenticated && isValidSession;
}

export async function logIn(username: string, password: string) {
  await Parse.User.logIn(username, password);
  let res = await checkUser();
  return res;
}

export async function signUp(username: string, password: string, name: string) {
  await Parse.User.signUp(username, password, {
    name
  });
  console.log('signUp', username, password);
  return checkUser();
}

export async function logOut() {
  const _logOut = async (attemp?: number): Promise<userType> => {
    attemp = attemp != null ? attemp : 0;
    try {
      let user = (await Parse.User.logOut()) as userType;
      return user;
    } catch (err) {
      if (attemp < 3) {
        return await _logOut(attemp + 1);
      }
      return null;
    }
  };
  return _logOut();
}

export async function checkUser(user?: userType) {
  user = user || (await getCurrentUser());
  if (user) {
    try {
      let serverUser = await new Parse.Query(Parse.User).get(user.id);
    } catch (err) {
      user = await logOut();
      throw err;
    }
  }
  return user;
}

export async function requestPasswordReset(login: string) {
  return (await Parse.User.requestPasswordReset(login)) as userType;
}
