import { LINQ } from 'berish-linq/dist';

export const ValidationModelSymbol = Symbol('[ValidationModelSymbol]');

class ValidationInfoModel {
  filters: FilterModel[] = [];
  isRequired = false;

  static createInstance(info: ValidationInfoModel) {
    if (info) {
      if (info instanceof ValidationInfoModel) return info;
      return Object.assign(new ValidationInfoModel(), info);
    }
    return new ValidationInfoModel();
  }

  getOrAddFilter(filter: FilterModel) {
    let index = this.filters.indexOf(filter);
    if (index === -1) index = this.filters.push(filter) - 1;
    return this.filters[index];
  }
}

interface IValidateModel {
  [ValidationModelSymbol]: {
    [propertyName: string]: ValidationInfoModel;
  };
}

class FilterModel {
  public condition: (value: any, target: any) => boolean;
  public errorText = 'Неверное значение';
  public isGetExecute = false;
  public isSetExecute = false;
  public isError = false;
  public isRequired = false;

  constructor(condition: (value: any, target: any) => boolean) {
    this.condition = condition;
  }

  get descriptor() {
    return (
      target: Object,
      propertyKey: string,
      descriptor: TypedPropertyDescriptor<any>
    ) => {
      let oldSet = descriptor.set!;
      let oldGet = descriptor.get!;
      let filterThis = this;
      descriptor.set = function(value: any) {
        let object = this as IValidateModel;
        object[ValidationModelSymbol] = object[ValidationModelSymbol] || {};
        let model = (object[ValidationModelSymbol][
          propertyKey
        ] = ValidationInfoModel.createInstance(
          object[ValidationModelSymbol][propertyKey]
        ));
        let filter = model.getOrAddFilter(filterThis);
        filter.isSetExecute = true;
        filter.isError = !filter.condition(value, this);
        model.isRequired = LINQ.fromArray(model.filters).any(m => m.isRequired);
        oldSet.call(this, value);
      };
      descriptor.get = function() {
        let value = oldGet.call(this);
        let object = this as IValidateModel;
        object[ValidationModelSymbol] = object[ValidationModelSymbol] || {};
        let model = (object[ValidationModelSymbol][
          propertyKey
        ] = ValidationInfoModel.createInstance(
          object[ValidationModelSymbol][propertyKey]
        ));
        let filter = model.getOrAddFilter(filterThis);
        filter.isError = !filter.condition(value, this);
        filter.isGetExecute = true;
        model.isRequired = LINQ.fromArray(model.filters).any(m => m.isRequired);
        return value;
      };
      return descriptor;
    };
  }
}

export function Validate<T>(m: T): LINQ<FilterModel>;
export function Validate<T>(m: T, propertyKey: string): ValidationInfoModel;
export function Validate<T>(
  m: T,
  propertyKey?: string
): LINQ<FilterModel> | ValidationInfoModel {
  let object = m as T & IValidateModel;
  object[ValidationModelSymbol] = object[ValidationModelSymbol] || {};
  if (propertyKey != null) {
    let model = (object[ValidationModelSymbol][
      propertyKey
    ] = ValidationInfoModel.createInstance(
      object[ValidationModelSymbol][propertyKey]
    ));
    return model;
  } else {
    let keys = Object.keys(object[ValidationModelSymbol]);
    return LINQ.fromArray(keys)
      .select(
        m =>
          (object[ValidationModelSymbol][
            m
          ] = ValidationInfoModel.createInstance(
            object[ValidationModelSymbol][m]
          ))
      )
      .selectMany(m => m.filters)
      .where(m => m.isError && m.isGetExecute);
  }
}

export function Property(
  ...decorators: ((
    target: Object,
    propertyKey: string,
    descriptor: TypedPropertyDescriptor<any>
  ) => TypedPropertyDescriptor<any>)[]
) {
  return function(target: any, key: string) {
    let descriptor = Object.getOwnPropertyDescriptor(target, key) || {};
    delete target[key];
    descriptor.set = function(val) {
      this['_' + key] = val;
    };
    descriptor.get = function() {
      return this['_' + key];
    };
    for (let dec of decorators) {
      descriptor = dec(target, key, descriptor);
    }
    Object.defineProperty(target, key, descriptor);
  };
}

export function Custom(
  condition: (value: any, target: any) => boolean,
  error?: string
) {
  let filter = new FilterModel(condition);
  filter.errorText = error || filter.errorText;
  return filter.descriptor;
}

export function IsRequired(error?: string) {
  let filter = new FilterModel(m => {
    if (typeof m == 'number') return m != null;
    return !!m;
  });
  filter.isRequired = true;
  filter.errorText = error || 'Поле обязательно для заполнения';
  return filter.descriptor;
}

export function IsRequiredWithCondition(
  cb: (model: any, value: any) => boolean,
  error?: string
) {
  let filter = new FilterModel((m, target) => {
    let res = cb(target, m);
    if (!res) return true;
    if (typeof m == 'number') return m != null;
    return !!m;
  });
  filter.isRequired = true;
  filter.errorText = error || 'Поле обязательно для заполнения';
  return filter.descriptor;
}

export function IsNotNull(error?: string) {
  let filter = new FilterModel(m => {
    if (typeof m == 'number') return m != null;
    return !!m;
  });
  filter.errorText = error || 'Поле не должно быть пустым';
  return filter.descriptor;
}

export function All(error?: string) {
  let filter = new FilterModel(m => {
    if (typeof m == 'number') return m != null;
    return !!m;
  });
  filter.errorText = error || 'Не все элементы удовлетворяют условию';
  return filter.descriptor;
}

export function IsPhone(error?: string) {
  // let pattern = /^8\d{10}$/;
  let filter = new FilterModel(
    v =>
      v
        ? `${v}`.startsWith('8') ||
          `${v}`.startsWith('7') ||
          `${v}`.startsWith('+7')
        : true
  );
  filter.errorText = error || 'Введите корректный номер телефона';
  return filter.descriptor;
}

export function Min(min: number, error?: string) {
  let filter = new FilterModel((v: string) => (v ? v.length >= min : true));
  filter.errorText = error || `Минимальная длина ${min}`;
  return filter.descriptor;
}

export function Max(max: number, error?: string) {
  let filter = new FilterModel((v: string) => (v ? v.length <= max : true));
  filter.errorText = error || `Максимальная длина ${max}`;
  return filter.descriptor;
}

export function Length(length: number, error?: string) {
  let filter = new FilterModel((v: string) => (v ? v.length == length : true));
  filter.errorText = error || `Длина поля ${length}`;
  return filter.descriptor;
}

export function IsEmail(error?: string) {
  let pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Zа-яА-Я\-0-9]+\.)+[a-zA-Zа-яА-Я]{2,}))$/;
  let filter = new FilterModel(v => (v ? pattern.test(v) : true));
  filter.errorText = error || 'Введите корректный email-адрес';
  return filter.descriptor;
}
