import * as React from 'react';
import {
  withStyles,
  TextField,
  Paper,
  WithStyles,
  MenuItem,
  Popper
} from '@material-ui/core';
import * as Autosuggest from 'react-autosuggest';
import styles from './styles';
import { FormItem, Tooltip, IFormItemProps } from '../abstract';

export interface IAutoCompleteProps {
  placeholder?: string;
  data?: string[];
  onFetch?: (value: string) => any;
  onChange?: (value: string) => any;
  defaultValue?: string;
}

interface IAutoCompleteState {
  value: string;
}

class AutoComplete extends React.Component<
  IAutoCompleteProps & WithStyles<typeof styles> & IFormItemProps,
  IAutoCompleteState
> {
  constructor(props) {
    super(props);
    this.state = {
      value: ''
    };
  }

  componentDidMount() {
    if (
      this.props.defaultValue != null &&
      this.props.defaultValue != this.state.value &&
      !this.state.value
    )
      this.onChange(this.props.defaultValue);
  }

  componentWillReceiveProps(nextProps: IAutoCompleteProps) {
    if (
      nextProps.defaultValue != null &&
      nextProps.defaultValue != this.state.value &&
      !this.state.value
    )
      this.onChange(nextProps.defaultValue);
  }

  onChange = (value: string) => {
    this.setState({ value });
    if (this.props.onChange) this.props.onChange(value);
  };

  onSuggestionsFetchRequested = ({ value }) => {
    if (this.props.onFetch) this.props.onFetch(value);
  };

  renderSuggestion = (
    value: string,
    params: Autosuggest.RenderSuggestionParams
  ) => {
    return (
      <MenuItem selected={params.isHighlighted} component="div">
        <div>
          <strong key={value} style={{ fontWeight: 300 }}>
            {value}
          </strong>
        </div>
      </MenuItem>
    );
  };

  renderInputComponent = (props: Autosuggest.InputProps<string>) => {
    const {
      ref,
      inputRef = () => {
        /* */
      },
      placeholder,
      ...other
    } = props;
    const input = (
      <TextField
        fullWidth
        placeholder={placeholder}
        InputProps={{
          inputRef: node => {
            ref(node);
            inputRef(node);
          }
        }}
        {...other as any}
      />
    );
    return Tooltip(input, {
      placeholder,
      trigger: 'hover'
    });
  };

  render() {
    const { data, classes, placeholder } = this.props;
    const { value } = this.state;

    return (
      <Autosuggest
        suggestions={data}
        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
        getSuggestionValue={(item: string) => item}
        renderSuggestion={this.renderSuggestion}
        renderInputComponent={this.renderInputComponent}
        theme={{
          container: classes.container,
          suggestionsContainerOpen: classes.suggestionsContainerOpen,
          suggestionsList: classes.suggestionsList,
          suggestion: classes.suggestion
        }}
        renderSuggestionsContainer={options => (
          <Paper {...options.containerProps} square>
            {options.children}
          </Paper>
        )}
        inputProps={{
          label: this.props.fiTitle,
          inputRef: node => {
            this['popperNode'] = node;
          },
          InputLabelProps: {
            shrink: true
          },
          placeholder: placeholder || 'Введите значение',
          value,
          onChange: (e, { newValue }) => this.onChange(newValue)
        }}
      />
    );
  }
}

export default FormItem(
  withStyles(styles)(AutoComplete) as React.ComponentClass<IAutoCompleteProps>,
  { fiTitleType: 'None' }
);
