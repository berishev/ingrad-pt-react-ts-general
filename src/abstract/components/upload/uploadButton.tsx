import * as React from 'react';
import { PageController } from '../../global/pageController';
import Button from '../button';
import { IOnUploadConfig } from './standart';
import uploadMethods from './uploadMethods';
import { FormItem } from '../abstract';
import Dropzone, { FileWithPreview } from 'react-dropzone';
import executeController from '../../global/executeController';

export interface IUploadButtonProps {
  placeholder?: string;
  title?: string;
  sizeMB?: number;
  onUploadMethod?: (file: FileWithPreview, config: IOnUploadConfig) => string;
  onUpload?: (file: string) => any;
  formats?: string[];
}

class UploadButton extends React.Component<IUploadButtonProps, {}> {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onChange = (value: string) => {
    if (this.props.onUpload) this.props.onUpload(value);
  };

  uploadRAW = (file: FileWithPreview) => {
    let config: IOnUploadConfig = {};
    config.formats = (this.props.formats || []).map(m => m.toUpperCase());
    config.sizeMB = this.props.sizeMB == 0 ? null : config.sizeMB || 2;
    let kb = file.size && file.size / 1024;
    if (config.sizeMB != null || kb > config.sizeMB * 1024)
      throw new Error('Файл должен быть меньше 2 МБ');
    const ext = (filename: string) => {
      if (filename.indexOf('.') == -1) return null;
      return /[.]/.exec(filename)
        ? /[^.]+$/.exec(filename)[0].toLowerCase()
        : undefined;
    };
    config.extension = (ext(file.name) || '').toUpperCase();
    if (
      (config.formats.length > 0 && config.extension == null) ||
      (config.formats.length > 0 &&
        config.formats.indexOf(config.extension) == -1)
    )
      throw new Error(`Файл должен быть формата: ${config.formats.join(', ')}`);
    if (this.props.onUploadMethod)
      return this.props.onUploadMethod(file, config);
    return uploadMethods.base64(file);
  };

  upload = async (files: FileWithPreview[]) => {
    let file = files[0];
    let res = await this.uploadRAW(file);
    this.onChange(res);
  };

  renderUploadButton = () => {
    return (
      <Button
        buttonTitle="Загрузить изображение"
        style={{ borderBottom: 'none' }}
        disableTooltip
      />
    );
  };

  onUploadButton = file => {
    setImmediate(() =>
      executeController.tryLoad(this.upload, { title: 'Загрузка файлов' }, file)
    );

    return false;
  };

  render() {
    let placeholder = this.props.placeholder || this.props.title;
    let input = (
      <Dropzone
        multiple={false}
        onDrop={(e, b) => this.onUploadButton(e)}
        style={{
          cursor: 'pointer',
          border: '1px dashed #d9d9d9',
          background: '#fafafa',
          width: '100%',
          height: '100%'
        }}
        className="upload-button"
      >
        {this.renderUploadButton()}
      </Dropzone>
    );
    return input;
  }
}

export default FormItem(UploadButton);
