import * as React from 'react';
import Dropzone, { FileWithPreview } from 'react-dropzone';
import Portal from 'berish-react-portals';
import { Visibility, Close, Inbox } from '@material-ui/icons';
import { Row, Col, Spin, Button } from '../';
import { AbstractComponent, FormItem, IFormItemProps } from '../abstract';
import uploadMethods from './uploadMethods';
import UploadModal from './uploadModal';
import executeController from '../..//global/executeController';

export interface IOnUploadConfig {
  formats?: string[];
  sizeMB?: number;
  extension?: string;
}

export interface IUploadProps extends IFormItemProps {
  onUploadMethod?: (
    file: FileWithPreview,
    config: IOnUploadConfig
  ) => string | Promise<string>;
  sizeMB?: number;
  placeholder?: string;
  formats?: string[];
}

interface IUploadState {
  file: FileWithPreview;
  loading: boolean;
}

class UploadStandart extends AbstractComponent<IUploadProps, IUploadState> {
  constructor(props) {
    super(props);
    this.state = {
      file: null,
      loading: false
    };
  }

  static defaultProps = {
    formats: ['png', 'jpg', 'jpeg']
  };

  onChange = (value: string) => {
    this.setValue(value);
  };

  getUrl = (file: string, uploadFile: FileWithPreview) => {
    return file;
    // return file || (uploadFile && (uploadFile.url || uploadFile.thumbUrl));
  };

  uploadRAW = (file: FileWithPreview) => {
    let config: IOnUploadConfig = {};
    config.formats = (this.props.formats || []).map(m => m.toUpperCase());
    config.sizeMB = this.props.sizeMB == 0 ? null : config.sizeMB || 2;
    let kb = file.size && file.size / 1024;
    if (config.sizeMB != null && kb > config.sizeMB * 1024)
      throw new Error('Файл должен быть меньше 2 МБ');
    const ext = (filename: string) => {
      if (filename.indexOf('.') == -1) return null;
      return /[.]/.exec(filename)
        ? /[^.]+$/.exec(filename)[0].toLowerCase()
        : undefined;
    };
    config.extension = (ext(file.name) || '').toUpperCase();
    if (
      (config.formats.length > 0 && config.extension == null) ||
      (config.formats.length > 0 &&
        config.formats.indexOf(config.extension) == -1)
    )
      throw new Error(`Файл должен быть формата: ${config.formats.join(', ')}`);
    if (this.props.onUploadMethod)
      return this.props.onUploadMethod(file, config);
    return uploadMethods.base64(file);
  };

  upload = async (files: FileWithPreview[]) => {
    let file = files[0];
    let res = await this.uploadRAW(file);
    this.setState({ file });
    this.onChange(res);
  };

  openModal = (url: string) => {
    return Portal.create(UploadModal)({ url });
  };

  renderModal = (file: string) => {
    let url = this.getUrl(file, this.state.file);
    if (url) {
      return (
        <div>
          <Row style={{ paddingTop: '5px', justifyContent: 'space-around' }}>
            <Col span={8}>
              <Button
                buttonTitle="Просмотр"
                icon={<Visibility />}
                onClick={() => this.openModal(url)}
              />
            </Col>
            <Col span={8}>
              <Button
                buttonTitle="Удалить"
                icon={<Close />}
                onClick={() => {
                  this.onChange(null);
                  this.setState({ file: null });
                }}
              />
            </Col>
          </Row>
        </div>
      );
    }
    return null;
  };

  renderUpload = (file: string) => {
    let url = this.getUrl(file, this.state.file);
    if (url) {
      return <img style={{ height: '150px', maxWidth: '402px' }} src={url} />;
    }
    let formats = (this.props.formats || []).map(m => m.toUpperCase());
    return (
      <div style={{ maxWidth: '402px', height: '150px', margin: '0 auto' }}>
        <Spin loading={!!this.state.loading} className="upload-loading">
          <Col
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              height: '100%'
            }}
          >
            <Row style={{ margin: 10 }}>
              <Inbox style={{ color: 'blue', fontSize: 64 }} />
            </Row>
            <Row style={{ margin: 5, textAlign: 'center', fontSize: 16 }}>
              Нажмите или перенесите файл в эту область для загрузки
            </Row>
            {formats && formats.length > 0 ? (
              <Row
                style={{
                  margin: 3,
                  textAlign: 'center',
                  fontSize: 12,
                  fontWeight: 200
                }}
              >
                Допустимые форматы файла: {formats.join(', ')}.
              </Row>
            ) : null}
          </Col>
        </Spin>
      </div>
    );
  };

  onUpload = file => {
    setImmediate(() =>
      executeController.tryLoad(
        this.upload,
        {
          changeLoading: loading => this.setState({ loading }),
          title: 'Загрузка файлова'
        },
        file
      )
    );
    return false;
  };

  render() {
    let value = this.getValue() as string;
    let disable = !!this.getUrl(value, this.state.file);
    let input = (
      <div>
        <Dropzone
          multiple={false}
          disabled={disable}
          onDrop={(e, b) => this.onUpload(e)}
          style={{
            cursor: 'pointer',
            border: '1px dashed #d9d9d9',
            background: '#fafafa',
            width: '100%',
            height: '100%'
          }}
        >
          {this.renderUpload(value)}
        </Dropzone>
        {this.renderModal(value)}
      </div>
    );
    return input;
  }
}

export default FormItem(UploadStandart, { fiTitleType: 'Divider' });
