import * as React from 'react';
import { Tabs, Tab } from '@material-ui/core';

export interface ITabProps<T> {
  label: string;
  value: T;
  children: React.ReactNode;
}

export interface ITabsProps<T> {
  value?: T;
  onChange?: (value: T) => any;
}

export class Child<T> extends React.Component<ITabProps<T>> {}

export class Root<T> extends React.Component<ITabsProps<T>> {
  constructor(props: ITabsProps<T>) {
    super(props);
    this.state = {};
  }

  renderData = () => {
    const data = React.Children.toArray(this.props.children) as JSX.Element[];
    const props = data.map(m => m.props) as ITabProps<T>[];
    const tabs = props.map((m, i) => <Tab label={m.label} value={i} key={i} />);
    const containers = props.map((m, i) => m.children);
    const values = props.map(m => m.value);
    const indexOf = values.indexOf(this.props.value);
    const currentIndex = indexOf <= 0 ? 0 : indexOf >= values.length ? (values.length - 1 <= 0 ? 0 : values.length - 1) : indexOf;
    const currentContainer = containers[currentIndex];
    return { tabs, containers, values, currentIndex, currentContainer };
  };

  onChange = (index: number) => {
    const data = React.Children.toArray(this.props.children) as JSX.Element[];
    const item = data[index];
    const props = item && (item.props as ITabProps<T>);
    const value = props && props.value;
    if (this.props.onChange) this.props.onChange(value);
  };

  render() {
    const data = this.renderData();
    return (
      <div style={{ flexGrow: 1, backgroundColor: 'white' }}>
        <Tabs value={data.currentIndex} onChange={(e, value) => this.onChange(value)}>
          {data.tabs}
        </Tabs>
        {data.currentContainer}
      </div>
    );
  }
}
