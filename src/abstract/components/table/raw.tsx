import * as React from 'react';
import { Table, Column, Cell, ColumnGroup } from 'fixed-data-table-2';
import 'fixed-data-table-2/dist/fixed-data-table.min.css';
import * as Dimensions from 'react-dimensions';
import { Checkbox } from '..';
import { storageController } from '../../global';
import Spin from '../spin';

export type CellStyleType<Value = any> =
  | React.CSSProperties
  | ((obj: Value, index: number) => React.CSSProperties);

interface IColumnBase<T> {
  title?: ((allItems: T[]) => React.ReactNode) | React.ReactNode;
  fixed?: boolean;
  width?: number;
  minWidth?: number;
  maxWidth?: number;
  flexGrow?: number;
}

export interface IColumnInput<T> extends IColumnBase<T> {
  children?: IColumnInput<T>[];
  render?: (obj: T, index: number) => React.ReactNode;
  isSorter?: boolean;
  sorter?: (obj?: T, index?: number) => any;
  rowStyle?: CellStyleType<T>;
  columnStyle?: CellStyleType<IColumnInput<T>>;
}

export interface IRawSelection<TValue> {
  items?: TValue[];
  onSelect?: (record: TValue, selected: boolean) => any;
  onSelectAll?: (records: TValue[], selected: boolean) => any;
  disabled?: boolean;
  on?: (records: TValue[]) => any;
}

export interface IRawProps<TValue> {
  columns: IColumnInput<TValue>[];
  dataSource: TValue[];
  prerender?: (value: React.ReactNode) => string | JSX.Element | JSX.Element[];
  loading?: boolean;
  title?: React.ReactNode | ((dataSource: TValue[]) => React.ReactNode);
  footer?: React.ReactNode | ((dataSource: TValue[]) => React.ReactNode);
  selection?: IRawSelection<TValue>;
  keyFunction?: (obj: TValue) => string;
  height?: number;
  width?: number;
  containerHeight?: number;
  containerWidth?: number;
  containerMinusHeight?: number;
  listenDrawer?: boolean;
  columnStyle?: CellStyleType<IColumnInput<TValue>>;
  rowStyle?: CellStyleType<TValue>;
}

interface IRawState {
  checkAll: boolean;
  drawerCollapsed: boolean;
}

class Raw<TValue> extends React.Component<IRawProps<TValue>, IRawState> {
  unlistener =
    this.props.listenDrawer &&
    storageController.localStore.subscribe(m => {
      let drawerCollapsed = m.collapsed;
      if (this.state.drawerCollapsed != drawerCollapsed)
        this.setState({ drawerCollapsed });
    });

  constructor(props: IRawProps<TValue>) {
    super(props);
    this.state = {
      checkAll: false,
      drawerCollapsed: storageController.localStore.collapsed
    };
  }

  componentWillUnmount() {
    if (this.unlistener) this.unlistener();
  }

  prerender = (value: React.ReactNode): React.ReactNode => {
    const { prerender } = this.props;
    if (prerender) return prerender(value);
    return value;
  };

  private getStyleColumn = (column: IColumnInput<TValue>, index: number) => {
    const { columnStyle: def } = this.props;
    const { columnStyle: custom } = column;
    const defaultStyle = typeof def == 'function' ? def(column, index) : def;
    const customStyle =
      typeof custom == 'function' ? custom(column, index) : custom;
    return Object.assign({}, defaultStyle || {}, customStyle || {});
  };

  private getStyleCell = (column: IColumnInput<TValue>, index: number) => {
    const { rowStyle: def } = this.props;
    const { rowStyle: custom } = column;
    const defaultStyle =
      typeof def == 'function' ? def(this.props.dataSource[index], index) : def;
    const customStyle =
      typeof custom == 'function'
        ? custom(this.props.dataSource[index], index)
        : custom;
    return Object.assign({}, defaultStyle || {}, customStyle || {});
  };

  renderColumn = (column: IColumnInput<TValue>, index: number): JSX.Element => {
    const header =
      typeof column.title == 'function'
        ? this.prerender(column.title(this.props.dataSource) as any)
        : this.prerender(column.title);
    const columnStyle = this.getStyleColumn(column, index);
    if (column.children && column.children.length > 0) {
      return (
        <ColumnGroup
          header={<Cell style={columnStyle}>{header}</Cell>}
          key={index}
          children={column.children.map((m, i) =>
            this.renderColumn(m, index * 10 + i)
          )}
        />
      );
    } else {
      return (
        <Column
          header={<Cell style={columnStyle}>{header}</Cell>}
          cell={props => {
            const rowStyle = this.getStyleCell(column, props.rowIndex);
            let result: React.ReactNode = null;
            if (column.render)
              result = column.render(
                this.props.dataSource[props.rowIndex],
                props.rowIndex
              );
            return (
              <Cell style={rowStyle} {...props}>
                {this.prerender(result)}
              </Cell>
            );
          }}
          width={column.width || 200}
          fixed={column.fixed}
          key={index}
          flexGrow={typeof column.flexGrow == 'undefined' ? 1 : column.flexGrow}
          minWidth={column.minWidth || 65}
          maxWidth={column.maxWidth}
        />
      );
    }
  };

  renderAllColumn = () => {
    const { columns, selection, keyFunction } = this.props;
    let beforeColumns: IColumnInput<TValue>[] = [];
    if (selection) {
      const checkboxColumn: IColumnInput<TValue> = {
        title: (
          <Checkbox
            fiDisable={true}
            disabledTooltip={true}
            value={
              selection.items.length == this.props.dataSource.length &&
              selection.items.length > 0
            }
            disabled={this.props.dataSource.length <= 0}
            onChange={value => {
              if (selection.onSelectAll)
                selection.onSelectAll(
                  value ? this.props.dataSource : [],
                  value
                );
            }}
          />
        ),
        render: item => {
          const index = selection.items
            .map(keyFunction)
            .indexOf(keyFunction(item));
          return (
            <Checkbox
              fiDisable={true}
              disabledTooltip={true}
              value={index != -1}
              onChange={value => {
                if (selection.onSelect) selection.onSelect(item, value);
              }}
            />
          );
        },
        width: 65,
        maxWidth: 65,
        flexGrow: 0,
        fixed: true
      };
      beforeColumns = [checkboxColumn];
    }
    return [...beforeColumns, ...columns].map((m, i) =>
      this.renderColumn(m, i)
    );
  };

  render() {
    const {
      containerHeight,
      containerWidth,
      containerMinusHeight
    } = this.props;
    const { drawerCollapsed } = this.state;
    // const width = drawerCollapsed
    //   ? containerWidth - drawerWidth - 80
    //   : containerWidth - drawerClosedWidth - 90;
    // const height = containerHeight - (containerMinusHeight || 220);
    const width = containerWidth;
    const height = containerHeight * 0.7;
    return (
      <Spin loading={this.props.loading}>
        <Table
          rowsCount={this.props.dataSource.length}
          rowHeight={75}
          headerHeight={50}
          width={width}
          height={height < 500 ? 500 : height}
        >
          {this.renderAllColumn()}
        </Table>
      </Spin>
    );
  }
}

type RawType = typeof Raw;

export default Dimensions({
  getHeight: function(element: HTMLElement) {
    return window.innerHeight;
  },
  getWidth: function(element) {
    return window.innerWidth;
  }
})(Raw) as RawType;
