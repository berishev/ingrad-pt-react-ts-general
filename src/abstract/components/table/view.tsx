import * as React from 'react';
import { Close, Visibility } from '@material-ui/icons';
import Portal from 'berish-react-portals';
import * as moment from 'moment';
import { Button, Row } from '../';
import { LINQ } from 'berish-linq/dist';
import TableModal from './modal';
import LINQTable from './linq';

import RcView, { IColumnInput, IRawSelection, IRawProps } from './raw';

export interface IColumn<T> extends IColumnInput<T> {
  children?: IColumn<T>[];
}

export interface ITableViewProps<TValue = any> extends IRawProps<TValue> {
  dataSource: TValue[];
  loading?: boolean;
  // pagination?: PaginationProps | false;
  columns: IColumn<TValue>[];
  pagination?: {};
  // onChangePagination?: (pagination: PaginationProps | boolean, filters: string[], sorter: Object) => any;
  onSort?: (col: IColumn<TValue>, type: 'down' | 'up') => any;
}

interface ITableViewState<T> {
  currentColumn: IColumn<T>;
}

export default class TableView<TValue> extends React.PureComponent<ITableViewProps<TValue>, ITableViewState<TValue>> {
  constructor(props) {
    super(props);
    this.state = {
      currentColumn: null
    };
  }

  prerender = (value: React.ReactNode): string | JSX.Element | JSX.Element[] => {
    if (value == null) return '-';
    let type = typeof value;
    if (type === 'string') return value as string;
    else if (type === 'number') return this.prerender(`${value}`);
    else if (type === 'boolean') return this.prerender(value == true ? 'Да' : 'Нет');
    else if (value instanceof Date)
      return this.prerender(
        moment(value as Date)
          .locale('ru')
          .format('MMMM Do YYYY, h:mm:ss')
      );
    else if (Array.isArray(value)) {
      let values = LINQ.fromArray(value).select(m => this.prerender(m));
      let isString = values.all(m => typeof m == 'string');
      return isString
        ? values
            .select(m => m as string)
            .toArray()
            .join('\n')
        : values.select(m => m as JSX.Element).toArray();
    } else return <span onClick={e => e.stopPropagation()}>{value as JSX.Element}</span>;
  };

  get selection() {
    return this.props.selection || {};
  }

  keyFunction = (m: TValue): string => (typeof m == 'object' && 'id' in m ? m['id'] : m);

  getRowSelectionConfig = () => {
    const selection = this.selection;
    if (selection.disabled) return null;
    let selectedItems = LINQ.fromArray(selection.items || []);
    const rawSelection: IRawSelection<TValue> = {
      items: selectedItems.toArray(),
      onSelect: (record, selected) => {
        if (selection.on) {
          if (selected) {
            selectedItems = selectedItems.concat(record);
          } else {
            selectedItems = selectedItems.where(m => this.keyFunction(m) != this.keyFunction(record));
          }
          selectedItems = selectedItems.distinct(this.keyFunction);
          return selection.on(selectedItems.toArray());
        }
      },
      onSelectAll: (records, selected) => {
        if (selection.on) {
          if (selected) {
            selectedItems = LINQ.fromArray([...records]);
          } else {
            selectedItems = LINQ.fromArray([]);
          }
          selectedItems = selectedItems.distinct(this.keyFunction);
          return selection.on(selectedItems.toArray());
        }
      }
    };
    return rawSelection;
  };

  renderSelectedField = () => {
    return (
      <Row style={{ margin: '0 0', justifyContent: 'center', alignItems: 'middle' }}>
        <span>Выбрано {this.selection.items.length} элементов</span>
        <Button
          icon={<Visibility />}
          style={{ marginLeft: '10px' }}
          shape="circle"
          placeholder="Посмотреть выбранные элементы"
          onClick={async () => {
            try {
              let TableClassModal = Portal.create(TableModal);
              let res = (await TableClassModal({
                tableClass: LINQTable,
                tableProps: {
                  data: this.selection.items || [],
                  pagination: false,
                  columns: this.props.columns
                }
              })) as TValue[];
              if (res && res.length > 0) {
                let rows = LINQ.fromArray(this.selection.items || []);
                rows = rows.except(res).distinct(m => (typeof m == 'object' && 'id' in m ? m['id'] : m));
                this.selection.on(rows.toArray());
              }
            } catch (err) {
              console.log(err);
            }
          }}
        />
        <Button icon={<Close />} style={{ marginLeft: '10px' }} shape="circle" placeholder="Очистить выбранные элементы" onClick={() => this.selection.on([])} />
      </Row>
    );
  };

  render() {
    return (
      <RcView
        {...this.props}
        dataSource={this.props.dataSource}
        columns={this.props.columns}
        prerender={this.prerender}
        loading={!!this.props.loading}
        title={!this.props.selection || true || !(this.selection.items && this.selection.items.length > 0) ? null : this.renderSelectedField}
        selection={this.props.selection && this.getRowSelectionConfig()}
        keyFunction={this.keyFunction}
        listenDrawer={true}
        containerMinusHeight={this.props.containerMinusHeight}
      />
    );
  }
}
