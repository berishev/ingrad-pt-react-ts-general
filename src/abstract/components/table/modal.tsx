import * as React from 'react';
import { IAbstractTableProps } from './abstract';
import * as Components from '../';
import { IStaticComponentProps } from 'berish-react-portals/dist/lib/portal';

interface IModalTableProps<T, TableProps extends IAbstractTableProps<T>> {
  tableClass: React.ComponentClass<TableProps>;
  tableProps: TableProps;
}

interface IModalTableState<T> {
  selected: T[];
}

export default class TableModal<
  T,
  TableProps extends IAbstractTableProps<T>
> extends React.Component<
  IStaticComponentProps<T[]> & IModalTableProps<T, TableProps>,
  IModalTableState<T>
> {
  constructor(props) {
    super(props);
    this.state = {
      selected: []
    };
  }

  resolve = () => {
    this.props.resolve(this.state.selected);
  };

  reject = () => {
    this.props.resolve();
  };

  renderFooterBar = () => {
    let left = [
      <Components.Button
        key="0"
        buttonTitle="Убрать элементы из выбранного"
        disabled={this.state.selected.length < 1}
        onClick={() => this.resolve()}
      />
    ];
    return <Components.ButtonBar left={left} />;
  };

  render() {
    let TableClass = this.props.tableClass;
    return (
      <Components.Modal.Form resolve={this.resolve} reject={this.reject}>
        <Components.ContentForm
          title="Выбранные элементы"
          footerBar={this.renderFooterBar()}
        >
          <TableClass
            {...this.props.tableProps as any}
            selection={{
              selected: this.state.selected,
              onSelect: selected => this.setState({ selected }),
              disabledShowSelected: true
            }}
          />
        </Components.ContentForm>
      </Components.Modal.Form>
    );
  }
}
