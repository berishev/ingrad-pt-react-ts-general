import AbstractConfig, { Object_Array_LINQ } from '../config';
type ConfigObject<T> = T[];
type FilterRequest<T> = T;
type FilterResponse = boolean;

export default class ArrayAbstractConfig<T> extends AbstractConfig<ConfigObject<T>, FilterRequest<T>, FilterResponse> {
  protected applyMethod() {
    let result = super.applyMethod();
    let linq = this.getLinq(result);
    if (this.filters != null && this.filters.length > 0) {
      let filters = this.getLinq(this.filters);
      linq = linq.where(m => {
        return filters.all(k => k(m));
      });
    }
    return linq.toArray();
  }
}
