import ArrayConfig from './array';
import ParseConfig from './parse';

export default { ArrayConfig, ParseConfig };
