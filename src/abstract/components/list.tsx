import * as React from 'react';
import { Row, Col, ArrayComponents, Button, Divider } from './';
import { AbstractComponent } from './abstract';

export interface IListComponentsProps<T> {
  title?: string;
  rowConfig?: {
    elementsAtRow?: number;
    emptyElements?: boolean;
  };
  showDeleteButton?: (index: number, object: T, ...args) => boolean;
  disableAddButton?: boolean;
  template: (
    index: number,
    object: T,
    afterSet: (object: any, path: string, value: any) => any,
    ...args
  ) => JSX.Element | JSX.Element[];
  args?: any[];
  onAdd?: () => void;
  onDelete?: (index: number, object: T, ...args) => void;
}

interface IListComponentsState {}

export default class ListComponents<T> extends AbstractComponent<
  IListComponentsProps<T>,
  IListComponentsState
> {
  renderDivider = () => {
    let title = this.props.title;
    if (title) return <Divider>{title}</Divider>;
    return null;
  };

  renderElement = (index: number, element: T, ...args) => {
    let el = this.props.template(
      index,
      element,
      (object: any) => this.onChangeItem(index, object),
      ...args
    );
    let showDeleteButton = true;
    if (this.props.showDeleteButton)
      showDeleteButton = this.props.showDeleteButton(index, element, ...args);
    let borderTopLeftRadius = '4px';
    let borderTopRightRadius = '4px';
    let borderBottomLeftRadius = '4px';
    let borderBottomRightRadius = '4px';
    return (
      <div key={index} style={{ paddingBottom: '20px' }}>
        <div
          key={index}
          style={{
            width: '100%',
            border: '1px solid #d9d9d9',
            padding: '20px',
            borderTopLeftRadius,
            borderTopRightRadius,
            borderBottomLeftRadius,
            borderBottomRightRadius
          }}
        >
          {showDeleteButton ? (
            <Row
              style={{
                margin: '0 0',
                justifyContent: 'space-around',
                alignItems: 'center'
              }}
            >
              <Col span={22}>
                <div
                  style={{
                    paddingRight: '5px',
                    borderRight: '1px solid #d9d9d9'
                  }}
                >
                  {el}
                </div>
              </Col>
              <Col
                span={2}
                style={{
                  textAlign: 'center',
                  paddingLeft: '5px',
                  alignItems: 'center'
                }}
              >
                <Button
                  fiTitle="Удалить"
                  placeholder="Удалить элемент"
                  onClick={() =>
                    this.props.onDelete &&
                    this.props.onDelete(index, element, ...args)
                  }
                />
              </Col>
            </Row>
          ) : (
            <Row
              style={{
                margin: '0 0',
                justifyContent: 'space-around',
                alignItems: 'center'
              }}
            >
              <Col span={24}>
                <div>{el}</div>
              </Col>
            </Row>
          )}
        </div>
      </div>
    );
  };

  renderAddButton = () => {
    let disable = this.props.disableAddButton;
    if (disable) return null;
    return (
      <Button
        buttonTitle="Добавить"
        placeholder="Добавить новый элемент"
        style={{ width: '100%' }}
        onClick={() => this.props.onAdd && this.props.onAdd()}
      />
    );
  };

  onChange = (value: T[]) => {
    this.setValue(value);
  };

  onChangeItem = (index: number, valueItem: T) => {
    let value = (this.getValue() as T[]) || [];
    value[index] = valueItem;
    this.onChange(value);
  };

  prepare<T>(items: T[], countAtRow: number, fillNull?: boolean) {
    let count = items.length;
    let rows: T[][] = [];
    let rowCount = Math.ceil(count / countAtRow);
    for (let i = 0; i < rowCount; i++) {
      rows[i] = items.slice(i * countAtRow, i * countAtRow + countAtRow);
      if (fillNull && rows[i].length < countAtRow) {
        for (let j = 0; j < countAtRow; j++) {
          if (typeof rows[i][j] === 'undefined') rows[i][j] = null;
        }
      }
    }
    return rows;
  }

  render() {
    let value = (this.getValue() as T[]) || [];
    let dataJSX: JSX.Element[] = ArrayComponents.render({
      elements: value,
      template: this.renderElement,
      args: this.props.args
    });
    if (this.props.rowConfig) {
      let elementsAtRow = this.props.rowConfig.elementsAtRow || 1;
      let fill = this.props.rowConfig.emptyElements || false;
      let data = this.prepare(value, elementsAtRow, fill);
      dataJSX = ArrayComponents.render({
        elements: data,
        template: (indexRow: number, elements: T[]) => {
          let colSpan = Math.floor(24 / elements.length);
          let valueElements = ArrayComponents.render({
            elements: elements,
            template: (indexCol: number, element: T, ...args) => (
              <Col span={colSpan}>
                {element == null
                  ? null
                  : this.renderElement(indexRow + indexCol, element, ...args)}
              </Col>
            ),
            args: this.props.args
          });
          return (
            <Row
              style={{
                margin: '0 0',
                justifyContent: 'space-around',
                alignItems: 'center'
              }}
            >
              {valueElements}
            </Row>
          );
        },
        args: this.props.args
      });
    }
    return (
      <>
        {this.renderDivider()}
        {dataJSX}
        {this.renderAddButton()}
        <Divider />
      </>
    );
  }
}
