import * as React from 'react';
import Divider from './divider';

interface IButtonBarProps {
  left?: JSX.Element[];
  right?: JSX.Element[];
  disableTopDivider?: boolean;
  disableBottomDivider?: boolean;
}

interface IButtonBarState {}

type AlignType = 'center' | 'left' | 'right';

export default class ButtonBar extends React.Component<IButtonBarProps, IButtonBarState> {
  renderButtons = (buttons: JSX.Element[], align: AlignType) => {
    buttons = buttons || [];
    return (
      <div
        style={{
          textAlign: align,
          width: '100%',
          padding: '10px 0'
        }}
      >
        {buttons.map((m, i) => (
          <div key={i} style={{ padding: '0 7px', display: 'inline-block' }}>
            {m}
          </div>
        ))}
      </div>
    );
  };

  render() {
    let cols: JSX.Element;
    if (this.props.left && this.props.right) {
      cols = (
        <div style={{ width: '100%' }}>
          <div style={{ flex: 1 }} key={0}>
            {this.renderButtons(this.props.left, 'left')}
          </div>
          <div style={{ flex: 1 }} key={1}>
            {this.renderButtons(this.props.right, 'right')}
          </div>
        </div>
      );
    } else if (this.props.left || this.props.right) {
      cols = this.renderButtons(this.props.left || this.props.right, 'center');
    }
    return (
      <div>
        {this.props.disableTopDivider ? null : <Divider isHrTag={true} />}
        <div style={{ display: 'flex', justifyContent: 'space-between', padding: '0 10px' }}>{cols}</div>
        {this.props.disableBottomDivider ? null : <Divider isHrTag={true} />}
      </div>
    );
  }
}
