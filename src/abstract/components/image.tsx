import * as React from 'react';
import { ButtonBase } from '@material-ui/core';
import { AbstractComponent, Tooltip } from './abstract';

export interface IImageProps {
  placeholder?: string;
  value?: string;
  selected?: boolean;
  onClick?: () => any;
  style?: React.CSSProperties;
}

export default class Image extends AbstractComponent<IImageProps> {
  constructor(props) {
    super(props);
  }

  render() {
    const { placeholder, value } = this.props;
    let url = value || this.getValue();
    const style = this.props.selected
      ? {
          boxShadow: '0 0 10px #000000',
          border: '2px solid white'
        }
      : {};
    let control = url && <img onClick={this.props.onClick} style={{ ...style, ...this.props.style }} src={url} />;
    return control && Tooltip(control, { trigger: 'hover', placeholder });
  }
}
