import * as React from 'react';
import Divider from './divider';

type ObjectOrFuncToGetObject<T> = (() => T) | T;

function getObjectInOOFTGO<T>(obj: ObjectOrFuncToGetObject<T>) {
  if (obj instanceof Function) return obj();
  return obj;
}

export interface IContentFormProps {
  title?: ObjectOrFuncToGetObject<React.ReactNode>;
  extraLeftTitle?: ObjectOrFuncToGetObject<React.ReactNode>;
  extraRightTitle?: ObjectOrFuncToGetObject<React.ReactNode>;
  headerBar?: ObjectOrFuncToGetObject<React.ReactNode>;
  footerBar?: ObjectOrFuncToGetObject<React.ReactNode>;
  disableTitle?: boolean;
}

export default class ContentForm extends React.Component<IContentFormProps, {}> {
  constructor(props) {
    super(props);
  }

  renderTitle = (title: React.ReactNode, left?: React.ReactNode, right?: React.ReactNode) => {
    return (
      <div
        style={{
          display: 'inline-flex',
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          width: '100%',
          padding: '10px 0'
        }}
      >
        <div
          style={{
            display: 'inline-flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            width: '15%'
          }}
        >
          {left}
        </div>
        <div
          style={{
            display: 'inline-flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            width: '70%'
          }}
        >
          {title}
        </div>
        <div
          style={{
            display: 'inline-flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            width: '15%'
          }}
        >
          {right}
        </div>
      </div>
    );
  };

  renderSubitle = () => {
    let { title, disableTitle, extraLeftTitle, extraRightTitle } = this.props;
    title = disableTitle ? null : getObjectInOOFTGO(title);
    if (title == null) return null;
    return this.renderTitle(typeof title == 'string' ? <h3>{title}</h3> : title, extraLeftTitle, extraRightTitle);
  };

  renderHeaderBar = () => {
    let { headerBar } = this.props;
    return headerBar ? <div style={{ width: '100%', display: 'inline' }}>{getObjectInOOFTGO(headerBar)}</div> : null;
  };

  renderFooterBar = () => {
    let { footerBar } = this.props;
    return footerBar ? <div style={{ width: '100%', display: 'inline' }}>{getObjectInOOFTGO(footerBar)}</div> : null;
  };

  render() {
    let headerBar = this.props.disableTitle ? null : this.renderHeaderBar();
    let title = this.props.disableTitle ? null : this.renderSubitle();
    title = title && (
      <div style={{ width: '100%' }}>
        {title}
        {!headerBar ? <Divider isHrTag={true} /> : null}
      </div>
    );
    let footerBar = this.renderFooterBar();
    return (
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'flex-start',
          alignItems: 'center',
          width: '100%',
          paddingTop: '10px'
        }}
      >
        {title || headerBar ? (
          <div
            style={{
              width: '100%',
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              paddingBottom: '10px'
            }}
          >
            {title}
            {headerBar}
          </div>
        ) : null}
        <div style={{ width: '100%' }}>
          <div style={{ overflowY: 'auto', overflowX: 'hidden', height: '100%' }}>
            <div style={{ width: '100%' }}>{this.props.children}</div>
          </div>
        </div>
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            width: '100%'
          }}
        >
          {footerBar}
        </div>
      </div>
    );
  }
}
