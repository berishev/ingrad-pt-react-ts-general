import * as React from 'react';
import { Button as AButton } from '@material-ui/core';
import { LINQ } from 'berish-linq/dist';
import Decorators from '../util/decorators';
import { Tooltip, IFormItemProps, FormItem } from './abstract';
import messagesController from '../global/messagesController';

export interface IButtonProps extends IFormItemProps {
  placeholder?: string;
  buttonTitle?: React.ReactNode;
  disableTooltip?: boolean;
  submit?: {
    validationObject: Object | Object[] | Object[][] | false;
    controller: React.Component;
  };
  onClick?: (event) => any;
  type?: 'inherit' | 'primary' | 'secondary' | 'default';
  size?: 'small' | 'medium' | 'large';
  shape?: 'circle' | 'circle-outline';
  style?: React.CSSProperties;
  loading?: boolean;
  icon?: JSX.Element;
  disabled?: boolean;
  variant?:
    | 'text'
    | 'flat'
    | 'outlined'
    | 'contained'
    | 'raised'
    | 'fab'
    | 'extendedFab';
}

class Button extends React.Component<IButtonProps, {}> {
  constructor(props: IButtonProps) {
    super(props);
  }

  getValidations<T>(objects: T | T[] | T[][]) {
    if (!objects) {
      return false;
    }
    if (objects instanceof Array) {
      if (objects.length <= 0) {
        return [];
      }
      return LINQ.fromArray(objects as Array<any>)
        .selectMany(m => (m instanceof Array ? m : [m]))
        .notNull()
        .toArray();
    } else {
      return [objects];
    }
  }

  onClick = event => {
    let onClick = this.props.onClick as any;
    if (onClick) onClick(event);
  };

  render() {
    let { buttonTitle, ...props } = this.props;
    let style = Object.assign(
      {},
      this.props.shape ? null : { width: '100%' },
      this.props.style || {}
    );
    let input = (
      <AButton
        {...props}
        style={style}
        size={this.props.size || 'large'}
        disabled={this.props.disabled}
        color={this.props.type}
        variant={this.props.variant}
        onClick={event => {
          let submit: {
            validationObject: false | Object | Object[] | Object[][];
            controller: React.Component;
          } = this.props.submit || ({} as any);
          let validationObject: Object[] | false = this.getValidations(
            submit.validationObject
          );
          if (validationObject == false) {
            this.onClick(event);
          } else {
            let filters = LINQ.fromArray(validationObject || []).selectMany(m =>
              Decorators.Filter.Validate(m).toArray()
            );
            let error = filters.any(m => m.isError);
            if (error) {
              for (let model of filters.where(m => m.isError).toArray()) {
                model.isSetExecute = true;
              }
              {
                messagesController.notification({
                  type: 'error',
                  message: 'Заполните все поля корректно'
                });
              }
              if (submit.controller) submit.controller.forceUpdate();
            } else {
              this.onClick(event);
            }
          }
        }}
      >
        {this.props.buttonTitle || this.props.fiTitle}
        {this.props.icon}
      </AButton>
    );
    return Tooltip(input, {
      placeholder:
        this.props.placeholder || this.props.buttonTitle || this.props.fiTitle,
      trigger: 'hover',
      disableTooltip: this.props.disableTooltip,
      isButton: true
    });
  }
}

export default FormItem(Button, { fiDisable: true });
