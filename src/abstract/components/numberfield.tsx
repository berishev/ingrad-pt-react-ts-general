import * as React from 'react';
import { Input, Icon } from '@material-ui/core';
import { AbstractComponent, Tooltip, IFormItemProps, FormItem } from './abstract';
import { InputProps } from '@material-ui/core/Input';

export interface ITextFieldProps extends IFormItemProps {
  placeholder?: string;
  regExp?: RegExp;
  min?: number;
  max?: number;
  step?: number;
  disabled?: boolean;
}

interface ITextfieldState {
  value: string;
}

class Textfield extends AbstractComponent<ITextFieldProps, ITextfieldState> {
  constructor(props) {
    super(props);
    this.state = {
      value: ''
    };
  }

  componentDidMount() {
    let value = this.getValue() || '';
    if (value != this.state.value) this.setState({ value });
  }

  componentWillReceiveProps(nextProps: ITextFieldProps) {
    let value = this.getValue(null, nextProps) || '';
    if (value != this.state.value) this.setState({ value });
  }

  clearValue = () => {
    this.onChange(null);
  };

  onChange = (value: string) => {
    if (!value && value != '0') {
      this.setValue(null);
    } else {
      let regExp = this.props.regExp;
      if (regExp) {
        if (!regExp.test(`${value}`)) {
          return;
        }
      }
      let num = parseInt(value, 10);
      this.setValue(num);
    }
  };

  renderSuffix = (value: any) => {
    return value ? <Icon onClick={this.clearValue}>close-circle</Icon> : null;
  };

  render() {
    const { value } = this.state;
    let placeholder = this.props.placeholder;
    let props: InputProps = {
      placeholder,
      value,
      onChange: e => this.onChange(e.target.value),
      disabled: !!this.props.disabled,
      type: 'number',
      endAdornment: !!this.props.disabled ? undefined : this.renderSuffix(value)
    };
    let input = <Input {...props} fullWidth={true} />;
    return Tooltip(input, {
      placeholder,
      trigger: 'focus'
    });
  }
}

export default FormItem(Textfield, { fiTitleType: 'InputLabel' });
