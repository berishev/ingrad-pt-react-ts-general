import * as React from 'react';
import { Checkbox as MCheckbox } from '@material-ui/core';
import { AbstractComponent, Tooltip, FormItem, IFormItemProps } from './abstract';

export interface ICheckboxProps extends IFormItemProps {
  placeholder?: string;
  disabled?: boolean;
  disabledTooltip?: boolean;
  value?: boolean;
  onChange?: (value: boolean) => any;
}

class Checkbox extends AbstractComponent<ICheckboxProps, {}> {
  constructor(props) {
    super(props);
  }

  onChange = (value: boolean) => {
    if (this.props.onChange) this.props.onChange(value);
    this.setValue(value);
  };

  render() {
    let value = typeof this.props.value == 'undefined' ? this.getValue() : this.props.value;
    let placeholder = this.props.placeholder || this.props.fiTitle;
    let input = (
      <MCheckbox disabled={this.props.disabled} checked={value} onChange={e => this.onChange(e.target.checked)} />
    );
    return this.props.disabledTooltip
      ? input
      : Tooltip(input, {
          placeholder: placeholder,
          trigger: 'hover'
        });
  }
}

export default FormItem(Checkbox, { fiTitleType: 'FormControlLabel' });
