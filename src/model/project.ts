import * as Parse from 'parse';
import * as collection from 'berish-collection';
import Decorators from 'berish-decorate';
import Enum from 'berish-enum';
import { EnumValueOf } from 'berish-enum/dist/lib';
import { IsRequired, Property } from '../abstract/util/decorators/filter';
import * as Model from './';

export const ProjectRelTypeEnum = Enum.createFromPrimitive('M', 'MO');
export type ProjectRelTypeEnumType = EnumValueOf<typeof ProjectRelTypeEnum>;

export const ProjectRelTypeLabels = new collection.Dictionary<
  ProjectRelTypeEnumType,
  string
>(
  new collection.KeyValuePair(ProjectRelTypeEnum.M, 'Москва'),
  new collection.KeyValuePair(ProjectRelTypeEnum.MO, 'Московская область')
);

export class Project extends Parse.Object {
  constructor() {
    super('Project');
  }

  get code() {
    return this.get('code');
  }

  set code(value: string) {
    this.set('code', value);
  }

  get name() {
    return this.get('name');
  }

  set name(value: string) {
    this.set('name', value);
  }

  get commercialName() {
    return this.get('commercialName');
  }

  set commercialName(value: string) {
    this.set('commercialName', value);
  }

  get iconUrl() {
    return this.get('iconUrl');
  }

  set iconUrl(value: string) {
    this.set('iconUrl', value);
  }

  get relType() {
    return ProjectRelTypeEnum[this.get('relType') as string];
  }

  set relType(value: ProjectRelTypeEnumType) {
    this.set('relType', ProjectRelTypeEnum[value]);
  }
}

Decorators.methodDecorate(Project, 'code', [IsRequired()]);
Decorators.methodDecorate(Project, 'name', [IsRequired()]);
Decorators.methodDecorate(Project, 'iconUrl', [IsRequired()]);
Decorators.methodDecorate(Project, 'relType', [IsRequired()]);

Parse.Object.registerSubclass('Project', Project);
