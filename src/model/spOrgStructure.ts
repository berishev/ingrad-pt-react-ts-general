import * as Parse from 'parse';

export class SPOrgStructure extends Parse.Object {
  static getQuery(query?: Parse.Query<SPOrgStructure>) {
    query = query || new Parse.Query(SPOrgStructure);
    query = query.descending('spID');
    return query;
  }

  constructor() {
    super('SPOrgStructure');
  }

  get spID() {
    return this.get('spID');
  }

  set spID(value: number) {
    this.set('spID', value);
  }

  get priority() {
    return this.get('priority');
  }

  set priority(value: number) {
    this.set('priority', value);
  }

  get isArchive() {
    return this.get('isArchive');
  }

  set isArchive(value: boolean) {
    this.set('isArchive', value);
  }

  get parentSPID() {
    return this.get('parentSPID');
  }

  set parentSPID(value: number) {
    this.set('parentSPID', value);
  }

  get LinkTitle() {
    return this.get('LinkTitle');
  }

  set LinkTitle(value: string) {
    this.set('LinkTitle', value);
  }

  get idEmployees() {
    return this.get('idEmployees');
  }

  set idEmployees(value: number) {
    this.set('idEmployees', value);
  }

  get assist() {
    return this.get('assist');
  }

  set assist(value: number) {
    this.set('assist', value);
  }
}

Parse.Object.registerSubclass('SPOrgStructure', SPOrgStructure);
