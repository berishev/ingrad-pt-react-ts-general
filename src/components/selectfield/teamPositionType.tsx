import * as React from 'react';
import * as Model from '../../model';
import { Selectfield } from '../../abstract/components';
import { ISelectFieldProps } from '../../abstract/components/selectfield';
import {
  AbstractComponent,
  IFormItemProps
} from '../../abstract/components/abstract';

interface IProps extends ISelectFieldProps<string>, IFormItemProps {
  team: Model.Team;
}

interface IState {}

export default class TeamPositionType extends AbstractComponent<
  IProps,
  IState
> {
  constructor(props) {
    super(props);
  }

  renderData = () => {
    if (!this.props.team) return [];
    return this.props.team.positions.map(m => {
      return {
        value: m,
        view: `${this.props.team.name} - ${m}`
      };
    });
  };

  render() {
    return (
      <Selectfield
        {...this.props}
        data={this.renderData()}
        placeholder="Выберите проектную роль"
      />
    );
  }
}
