import * as React from 'react';
import * as Model from '../../model';
import { Selectfield } from '../../abstract/components';
import { ISelectFieldProps } from '../../abstract/components/selectfield';
import {
  AbstractComponent,
  IFormItemProps
} from '../../abstract/components/abstract';

interface IProps
  extends ISelectFieldProps<Model.ProjectRelTypeEnumType>,
    IFormItemProps {}

interface IState {}

export default class ProjectRelType extends AbstractComponent<IProps, IState> {
  constructor(props) {
    super(props);
  }

  renderData = () => {
    return Model.ProjectRelTypeLabels.toLinq().select(m => {
      return {
        view: `${Model.ProjectRelTypeEnum[m.key]} - ${m.value}`,
        value: m.key
      };
    });
  };

  render() {
    return (
      <Selectfield
        {...this.props}
        data={this.renderData()}
        placeholder="Выберите город/область"
      />
    );
  }
}
