import * as React from 'react';
import * as Parse from 'parse';
import QueryLINQ from 'berish-parse-query-linq';
import { LINQ } from 'berish-linq/dist';
import * as Model from '../../model';
import { Selectfield } from '../../abstract/components';
import { ISelectFieldProps } from '../../abstract/components/selectfield';
import {
  AbstractComponent,
  IFormItemProps
} from '../../abstract/components/abstract';
import executeController from '../../abstract/global/executeController';

interface IProps extends ISelectFieldProps<Model.Team>, IFormItemProps {}

interface IState {
  data: LINQ<Model.Team>;
}

export default class extends AbstractComponent<IProps, IState> {
  constructor(props) {
    super(props);
    this.state = {
      data: LINQ.fromArray([])
    };
  }

  componentDidMount() {
    executeController.tryLoad(this.onLoad);
  }

  onLoad = async (nextProps?: IProps) => {
    let query = new Parse.Query(Model.Team);
    let data = await QueryLINQ(query);
    this.setState({
      data
    });
  };

  renderData = () => {
    let { data } = this.state;
    return data.select(m => {
      return {
        value: m,
        view: m.fullname ? `${m.name} - ${m.fullname}` : `${m.name}`
      };
    });
  };

  render() {
    return (
      <Selectfield
        placeholder="Выберите отдел"
        data={this.renderData()}
        {...this.props}
      />
    );
  }
}
