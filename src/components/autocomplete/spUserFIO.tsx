import * as React from 'react';
import * as Parse from 'parse';
import * as Model from '../../model';
import { AutoComplete } from '../../abstract/components';
import { IAutoCompleteAsyncProps } from '../../abstract/components/autoComplete/async';
import ParseAbstractConfig from '../../abstract/components/filter/configs/parse';
import AbstractConfig from '../../abstract/components/filter/config';

export interface IProps extends IAutoCompleteAsyncProps {
  onSelect?: (item: Model.SPUser) => any;
}

export default class extends React.Component<IProps> {
  onFetch = async (value: string) => {
    let query = Model.SPUser.getQuery();
    let queries = value
      .split(' ')
      .map(m =>
        ParseAbstractConfig.parseOr(
          AbstractConfig.clone(query).matches(
            'lastname',
            new RegExp(m, 'i'),
            null
          ),
          AbstractConfig.clone(query).matches(
            'firstname',
            new RegExp(m, 'i'),
            null
          ),
          AbstractConfig.clone(query).matches(
            'patronymic',
            new RegExp(m, 'i'),
            null
          )
        )
      );
    const endQuery = ParseAbstractConfig.parseAnd(...queries).limit(5);
    const items = await endQuery.find();
    return items.map(m => ({ value: m, text: m.LinkTitle }));
  };

  onSelect = async (text: string, value: Model.SPUser) => {
    if (this.props.onSelect) this.props.onSelect(value);
  };

  render() {
    return (
      <AutoComplete.Async
        {...this.props}
        onFetch={this.onFetch}
        onSelect={this.onSelect}
      />
    );
  }
}
