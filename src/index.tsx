import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './styles';
import App from './mvc';

ReactDOM.render(<App />, document.getElementById('root') as HTMLElement);
